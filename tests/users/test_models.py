from api.users.models import Users
from sqlalchemy import select


def test_create_user(db):
    user = Users(
        password="test",
        email="test@email.com",
        created_at="2021-01-01",
        updated_at="2021-01-01",
        first_name="test",
        last_name="test",
    )
    with db() as session:
        session.add(user)
        session.commit()
        assert user.id == 1
        assert user.password == "test"
        assert user.email == "test@email.com"
        assert user.created_at == "2021-01-01"
        assert user.updated_at == "2021-01-01"
        assert user.first_name == "test"
        assert user.last_name == "test"



def test_delete_user(db):
    with db.session() as session:
        users = session.execute(select(Users)).scalars().all()
        if len(users > 1):
            user = users[1]
            try:
                session.delete(user)
            except Exception as e:
                print(f"User cold not be deleted: {e}")
        users = session.execute(select(Users)).scalars().all()