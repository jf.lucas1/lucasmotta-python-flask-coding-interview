from typing import Optional

from pydantic import BaseModel

from flask_openapi3 import APIBlueprint
from sqlalchemy import select

from database import db
from api.users.models import Users
from flask import request
from flask.json import jsonify

users_app = APIBlueprint("users_app", __name__)


class UserSchema(BaseModel):
    id: int
    password: str
    email: str
    created_at: str
    updated_at: str
    last_login: str
    first_name: str
    last_name: str


class UserList(BaseModel):
    users: list[UserSchema]


@users_app.get("/users", responses={"200": UserList})
def get_users():
    with db.session() as session:
        users = session.execute(select(Users)).scalars().all()
        return jsonify(users)


@users_app.post("/users", responses={"201": UserSchema})
def create_user():
    with db.session() as session:
        user = Users(
            password=request.data("password"),
            email=request.data("email"),
            first_name=request.data("first_name"),
            last_name=request.data("last_name"),
        )
        session.add(user)
        session.commit()
        return user, 201
    

@users_app.get("/users/<id>", responses={"200": UserSchema})
def get_user(id):
    with db.session() as session:
        try:
            user = session.execute(select(Users).where(Users.id==id)).scalar_one()
        except Exception as e:
            print(f"Couldn't get user with id: {id}")
            return None, 404
        else:
            return user, 200
            
            
            